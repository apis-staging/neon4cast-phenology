#!/usr/bin/env bash

IMAGE=${1:-cpu}
TAG=${2:-latest}

GROUP=apis-staging

if [ $IMAGE == "cpu" ]; then
		PORT=8888
		RUNTIME=""
		PROJECT=$(basename $(pwd))
		CRED=$PROJECT
		DATADRIVE="$(pwd)/data"
elif [ $IMAGE == "gpu" ]; then
		PORT=8999
		RUNTIME="--runtime=nvidia"
		PROJECT=greenwave
		CRED=greenwave
		DATADRIVE="/datadrive"
else
    echo "Error: argument must be one of 'cpu' or 'gpu', you passed '$IMAGE'."
    exit 1
fi
ACCESS_POINT=http://localhost:$PORT/

IMAGE_NAME=registry.gitlab.com/$GROUP/$PROJECT/$IMAGE:$TAG

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
  echo "Image not found, building from recipe...."
  ./$(find . -path \*build.sh) $IMAGE $TAG
fi

echo "$ACCESS_POINT (with usr and pwd '$CRED')"

docker run $RUNTIME -d -it --rm \
		--name rstudio-$PROJECT-$IMAGE-$TAG \
		-v "$(pwd)":/home/$CRED \
		-v $DATADRIVE:/datadrive \
		-w /home/$CRED \
		-e USER=$CRED \
		-e PASSWORD=$CRED \
		-p $PORT:8787 \
		$IMAGE_NAME

rm -rf kitematic/ .rstudio/ rstudio/
